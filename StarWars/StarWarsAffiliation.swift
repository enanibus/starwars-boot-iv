//
//  StarWarsAffiliation.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 1/17/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import Foundation

enum StarWarsAffiliation : String{
    
    case galacticEmpire     = "Galactic Empire"
    case rebelAlliance      = "Rebel Alliance"
    case firstOrder         = "First Order"
    case jabbaCriminalEmpire = "Jabba's Criminal Empire"
    case unknow             = "Unknown"
    
    
    static func by(name: String) -> StarWarsAffiliation{
        
        let aff : StarWarsAffiliation
        
        switch name {
        case StarWarsAffiliation.galacticEmpire.rawValue :
            aff = .galacticEmpire
            
        case StarWarsAffiliation.rebelAlliance.rawValue :
            aff = .rebelAlliance
            
        case StarWarsAffiliation.firstOrder.rawValue :
            aff = .firstOrder
            
        case StarWarsAffiliation.jabbaCriminalEmpire.rawValue :
            aff = .jabbaCriminalEmpire
        default:
            aff = .unknow
        }
        
        return aff
    }
    
    static func by(name: String?) -> StarWarsAffiliation{
        
        guard let realName = name else{
            return .unknow
        }
        
        return by(name: realName)
    }
    
    
    
    
    
    
    
    
    
    
    
    
    
    
}

