//
//  CharacterViewController.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 1/24/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import UIKit
import AVFoundation

class CharacterViewController: UIViewController {

    //MARK: - Properties
    @IBOutlet weak var photoView: UIImageView!
    var model : StarWarsCharacter
    var player : AVAudioPlayer?
    
    //MARK: - Initialization
    init(model: StarWarsCharacter){
        self.model = model
        super.init(nibName: nil,
                   bundle: nil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - View Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        syncViewWithModel()
        
    }
    
    
    //MARK: - Sync model -> View
    func syncViewWithModel(){
        photoView.image = model.photo
        title = model.alias ?? model.name
        
    }
    //MARK: - Actions
    @IBAction func playSound(_ sender: UIBarButtonItem) {
       
        do{
            
            // Avisamos que vamos a reproducir sonido
            try AVAudioSession.sharedInstance().setCategory(AVAudioSessionCategoryPlayback)
            try AVAudioSession.sharedInstance().setActive(true)
            
            // Le decimos qué formato de sonido vamos a reproducir
            player = try AVAudioPlayer(data: model.soundData,
                                       fileTypeHint: AVFileTypeCoreAudioFormat)
            // Pos habrá que reproducir el sonido
            player?.play()
            
        }catch let err as NSError{
            print("error reproduciendo sonido")
            print("\(err)")
            
        }
        
        
    }
    
    
    @IBAction func displayWikipedia(_ sender: UIBarButtonItem) {
        
        // Crear un WikiVC
        let wVC = WikiViewController(model: model)
        
        // Hacer un push
        navigationController?.pushViewController(wVC, animated: true)
        
    }
}

//MARK: - Protocols
extension CharacterViewController : UniverseTableViewControllerDelegate{
    
    
    func universeTableViewController(_ uVC: UniverseTableViewController, didSelectCharacter char: StarWarsCharacter) {
        
        
        // Cambiamos el modelo
        model = char
        syncViewWithModel()
        
        
        
    }
    
}





















