//
//  Errors.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 1/19/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import Foundation

enum StarWarsError : Error{
    case wrongURLFormatForJSONResource
    case resourcePointedByURLNotReachable
    case wrongJSONFormat
    case nilJSONObject
    case jsonParsingError
}

