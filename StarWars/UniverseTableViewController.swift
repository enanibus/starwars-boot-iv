//
//  UniverseTableViewController.swift
//  StarWars
//
//  Created by Fernando Rodríguez Romero on 1/23/17.
//  Copyright © 2017 Keepcoding. All rights reserved.
//

import UIKit

class UniverseTableViewController: UITableViewController {
    
    //MARK: - Constants
    static let notificationName = Notification.Name(rawValue: "CharacterDidChange")
    static let characterKey = "CharacterKey"
    
    
    //MARK: - Properties
    let model : StarWarsUniverse
    
    weak var delegate : UniverseTableViewControllerDelegate? = nil
    
    
    //MARK: - Initialization
    init(model: StarWarsUniverse){
        self.model = model
        super.init(nibName: nil, bundle: nil)
        
    }
    

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    //MARK: - Table View Delegate
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        // descubrir la afiliación
        let aff = getAffiliation(forSection: indexPath.section)
        
        // descubrir de qué personaje me hablas
        let char  = model.character(atIndex: indexPath.row, forAffiliation: aff)
        
        // Avisar al delegado
        delegate?.universeTableViewController(self, didSelectCharacter: char)
        
        // mandamos una notificación
        notify(characterChanged: char)
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        //Incomplete implementation, return the number of sections
        return model.affiliationCount
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //Cuantos personajes hay en esta aff??
        return model.characterCount(forAffiliation: getAffiliation(forSection: section))
    }

    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        
        return model.affiliationName(getAffiliation(forSection: section))
    }
    
    override func tableView(_ tableView: UITableView,
                            cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        // Definir un id para el tipo de celda
        let cellId = "StarWarsCell"
        
        // Averiguar la afiliación
        let aff = getAffiliation(forSection: indexPath.section)
        
        // Averiguar quién es el personaje
        let char = model.character(atIndex: indexPath.row, forAffiliation: aff)
        
        // Crear la celda
        var cell = tableView.dequeueReusableCell(withIdentifier: cellId)
        
        if cell == nil{
            // El opcional está vacio y toca crear
            // la celda desde cero
            cell = UITableViewCell(style: .subtitle,
                                   reuseIdentifier: cellId)
        }
        // Configurarla
        cell?.imageView?.image = char.photo
        cell?.textLabel?.text = char.alias
        cell?.detailTextLabel?.text = char.name

        // Devolverla
        return cell!
       
    }
    
    
    //MARK: - Utils
    func getAffiliation(forSection section: Int) -> StarWarsAffiliation{
        
        var aff: StarWarsAffiliation = .unknow
        
        switch section {
        case 0:
            aff = .galacticEmpire
            
        case 1:
            aff = .rebelAlliance
            
        case 2:
            aff = .jabbaCriminalEmpire
            
        case 3:
            aff = .firstOrder
        default:
            aff = .unknow
        }
        
        return aff
    }




}

//MARK: - UniverseTableViewControllerDelegate
protocol UniverseTableViewControllerDelegate : class {
    
    // métodos should (piden permiso)
    // métodos will (lo viá hacer)
    // métodos did (ya lo he hecho. que lo sepasss)
    func universeTableViewController(_ uVC: UniverseTableViewController,
                                     didSelectCharacter char: StarWarsCharacter)
    
}

//MARK: - Notifications
extension UniverseTableViewController{
    
    func notify(characterChanged char: StarWarsCharacter){
        
        // "Creas" una instancia del NotificationCenter
        let nc = NotificationCenter.default
        
        // Creas un objeto nitification
        let notification = Notification(name: UniverseTableViewController.notificationName, object: self, userInfo: [UniverseTableViewController.characterKey : char])
        
        // Lo mandas
        nc.post(notification)
    }
}




















